# BadGuyBot's DeckBuilderAssets #

### Assets ###

* In this repo are the images/assets used to build the Deck images for BadGuyBot.
* Current version of images are from 1.23 (I think)

### To Do ###

1. Balance changes from 1.24 (mostly power cost changes)
2. Cards from Horus Traver Adventure need to be added.

### Issues ###

* The special character in Diogo's name has been converted due to limitation in the bot's code.

### Helping out ###

* Please open a new issue for any card discrepancies like misspellings or incorrect power cost.